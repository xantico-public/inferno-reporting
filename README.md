# Inferno reporting

Inferno adds simple but powerful custom reporting to any web application!

  - Add report tables or charts directly into your page flow
  - Drag and drop UI makes it possible to build reports with limited SQL knowledge
  - Integration is as simple as including the microservice script tag in each page you want reports on

### Version
0.0.1

### Installation

Built in Zend expressive and Angular?

```sh
$ composer install
```

### Development

Want to contribute? Great!

Add details here...

### Todos

 - Everything
 
License
----

MIT
